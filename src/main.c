#define HEAP_SIZE 4096
#define TEST_SIZE 500
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdlib.h>

void remove_heap(void* heap){
    munmap(heap, size_from_capacity((block_capacity) {.bytes=HEAP_SIZE}).bytes);
}
void test_heap(int num, void* heap){
    fprintf(stderr, "heap from test №%d:\n",num);
    debug_heap(stderr, heap);
}

void* check_heap_init(int size){
    void* heap = heap_init(size);
    if (!heap){
        fprintf(stderr, "error heap_init");
        exit(1);
    }
    return heap;
}

void* check_malloc(int size){
    void *block = _malloc(size);
    if (!block){
        fprintf(stderr, "error _malloc");
        exit(1);
    }
    return block;

}
void test_alloc() {
    struct block_header *heap = check_heap_init(HEAP_SIZE);
    if (!heap)
        exit(1);
    void *block = check_malloc(TEST_SIZE);
    if (!block)
        exit(1);
    test_heap(1, heap);
    _free(block);
    test_heap(1, heap);
    remove_heap(heap);
}

void test_1_block() {
    struct block_header *heap = check_heap_init(HEAP_SIZE);
    void *block1 = check_malloc(TEST_SIZE);
    void *block2 = check_malloc(TEST_SIZE);
    void *block3 = check_malloc(TEST_SIZE);
    test_heap(2, heap);
    _free(block3);
    test_heap(2, heap);
    _free(block1);
    _free(block2);
    test_heap(2, heap);
    remove_heap(heap);
}

void test_2_blocks() {
    struct block_header *heap = check_heap_init(HEAP_SIZE);
    void *block1 = check_malloc(TEST_SIZE);
    void *block2 = check_malloc(TEST_SIZE);
    void *block3 = check_malloc(TEST_SIZE);
    test_heap(3, heap);
    _free(block1);
    _free(block2);
    test_heap(3, heap);
    _free(block3);
    test_heap(3, heap);
    remove_heap(heap);
}

void test_block_over() {
    struct block_header *heap = check_heap_init(HEAP_SIZE);
    test_heap(4, heap);
    void *block = check_malloc(10*TEST_SIZE + HEAP_SIZE);
    test_heap(4, heap);
    _free(block);
    test_heap(4, heap);
    remove_heap(heap);
}

void test_block_over_error() {
    void *heap = check_heap_init(HEAP_SIZE);
    test_heap(5, heap);
    void *block1 = check_malloc(HEAP_SIZE);
    test_heap(5, heap);
    struct block_header *next_block = block1 - offsetof(struct block_header, contents);
    void *m = mmap((next_block->contents + next_block->capacity.bytes), HEAP_SIZE, PROT_READ | PROT_WRITE,MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    void *block2 = check_malloc(TEST_SIZE);
    test_heap(5, heap);
    _free(block1);
    _free(block2);
    test_heap(5, heap);
    remove_heap(m);
    remove_heap(heap);
}
int main() {
    printf("Run 5 tests");
    test_alloc();
    test_1_block();
    test_2_blocks();
    test_block_over();
    test_block_over_error();
    return 0;
}
